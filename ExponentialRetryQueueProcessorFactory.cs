﻿using Microsoft.Azure.WebJobs.Host.Executors;
using Microsoft.Azure.WebJobs.Host.Queues;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.WebJobs
{
	/// <inheritdoc />
	/// <summary>
	/// https://github.com/Azure/azure-webjobs-sdk-samples/blob/master/BasicSamples/MiscOperations/CustomQueueProcessorFactory.cs
	/// </summary>
	public class ExponentialRetryQueueProcessorFactory : IQueueProcessorFactory
	{
		private readonly TimeSpan _visibilityTimeout;
		private readonly int _maxDequeueCount;

		public ExponentialRetryQueueProcessorFactory(TimeSpan visibilityTimeout, int maxDequeueCount)
		{
			_visibilityTimeout = visibilityTimeout;
			_maxDequeueCount = maxDequeueCount;
		}

		public QueueProcessor Create(QueueProcessorFactoryContext context)
		{
			// Note: maximum timeout = VisibilityTimeout ^ (MaxDequeueCount - 1)
			context.VisibilityTimeout = _visibilityTimeout;
			context.MaxDequeueCount = _maxDequeueCount;
			return new CustomQueueProcessor(context);
		}

		private class CustomQueueProcessor : QueueProcessor
		{
			public CustomQueueProcessor(QueueProcessorFactoryContext context)
				: base(context)
			{
			}

			protected override async Task ReleaseMessageAsync(CloudQueueMessage message, FunctionResult result, TimeSpan visibilityTimeout, CancellationToken cancellationToken)
			{
				// VisibilityTimeout ^ DequeueCount
				var exponentialVisibilityTimeout = TimeSpan.FromSeconds(Math.Pow(visibilityTimeout.TotalSeconds, message.DequeueCount));
				await base.ReleaseMessageAsync(message, result, exponentialVisibilityTimeout, cancellationToken);
			}
		}
	}
}