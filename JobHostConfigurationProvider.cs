﻿using Autofac;
using Microsoft.Azure.WebJobs;
using StudioKit.Configuration;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Interfaces;
using System;

namespace StudioKit.Cloud.WebJobs
{
	public abstract class JobHostConfigurationProvider
	{
		private readonly string _name;

		protected JobHostConfigurationProvider(string name)
		{
			_name = name;
		}

		public IContainer Container { get; set; }

		public virtual JobHostConfiguration Configuration()
		{
			var builder = CreateAndConfigureBuilder();
			Container = builder.Build();
			ConfigureServices(Container);

			var config = new JobHostConfiguration
			{
				JobActivator = new AutofacJobActivator(Container),
				Queues =
				{
					QueueProcessorFactory = new ExponentialRetryQueueProcessorFactory(TimeSpan.FromSeconds(4), 5)
				}
			};

			if (config.IsDevelopment)
			{
				config.UseDevelopmentSettings();
			}

			return config;
		}

		protected ContainerBuilder CreateAndConfigureBuilder()
		{
			var builder = new ContainerBuilder();
			RegisterServices(builder);
			return builder;
		}

		protected virtual void RegisterServices(ContainerBuilder builder)
		{
			// Singletons
			builder.Register(c => new Logger(_name))
				.AsSelf()
				.As<ILogger>()
				.SingleInstance();
			builder.Register(c => new SentryErrorHandler(
					EncryptedConfigurationManager.GetSetting(BaseAppSetting.SentryDsn),
					new DefaultJsonPacketFactory()))
				.As<IErrorHandler>()
				.SingleInstance();
		}

		protected abstract void ConfigureServices(IContainer container);
	}
}